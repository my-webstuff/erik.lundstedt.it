#!/usr/bin/env fennel
(local devmode nil)
;;; stuff to make fennel work
(local fennel (require :fennel))
(table.insert (or package.loaders package.searchers) fennel.searcher)
(local vars (require :vars))
(set fennel.path (.. fennel.path ";../hydra/?.fnl"))

;;; imports
(local tags (require :hydra.tags))

;; keeping stuff organized
(local hydra (require :hydra.hydra))
(local misc (require :hydra.misc))
(local util (require :util))
(local defaults (require :defaults))
;;; initialize a table to stuff data in
;;- this sepresents the highest level of the page
(local page {})

;; more effort to get stuff organized
;;;; new:start using a settings table
(local settings {:title :newtab :url :www/pages/newtab.html})

;;; set some metadata
;;;; variable to store metadata:charset
(local charset "charset=\"UTF-8\"")
;;;; set the "head of the page-data"
(set page.head
	 [(tags.head
	   [(tags.stylesheet :/styles/index.css)
		(tags.title settings.title)
		(tags.meta charset)])])

;;; fetch data from util
(local navigation util.navigation)
;;(tset page navigation (util.navigation))
;;(print (util.dump navigation))
;;;; modify the navigation table
;;(tset navigation settings.title  {:text (. navigation settings.title :text)	   :url (. navigation settings.title :url)	   :class :focused})

(tset navigation settings.title :class :focused)

(util.log "# # # # # # # # # # # # # # # # # # # # # #" devmode)
(util.log (util.dump navigation) devmode)
(util.log "# # # # # # # # # # # # # # # # # # # # # #" devmode)
(util.log (tags.navigation navigation) devmode)

;;; linktables
;;;; usefull links
(local quickLinks
	   [;;{:text :url}
		;;{:text :url}
		;;{:text :url}
		;;{:text :url}
		;;{:text :url}
		;;{:text :url}
		{:text :DuckDuckGo :url "https://duckduckgo.com"}
		{:text "lua docs" :url "https://www.lua.org/manual/5.4/index.html"}
		{:text "orgmode manual" :url "https://orgmode.org/manual/Main-Index.html"}
		{:text "org manual^1" :url "https://orgmode.org/worg/"}
		{:text "org manual^2" :url "https://orgmode.org/worg/dev/org-syntax.html"}
		{:text :wikipedia :url "https://en.m.wikipedia.org/"}
		{:text "arch wiki" :url "https://wiki.archlinux.org/index.php"}
		{:text "gnu manpages" :url "https://linux.die.net/man/"}
		{:text "explain shell" :url "https://explainshell.com"}])

;;;; luakit
(local luakitLinks
	   [{:text "luakit homepage" :url "https://luakit.github.io/"}
		{:text "luakit api page" :url "https://luakit.github.io/docs"}
		{:text "luakit settings" :url "luakit://settings"}
		{:text "luakit bookmarks" :url "luakit://bookmarks"}
		{:text "luakit history" :url "luakit://history"}
		{:text "luakit binds" :url "luakit://binds"}
		{:text "luakit log" :url "luakit://log"}
		{:text "luakit addblock" :url "luakit://addblock"}
		{:text "this page" :url "https://erik.lundstedt.it/pages/newtab.html"}])

;;;; awesome windowmanager
(local awesomewmLinks
	   [{:text "awesomewm homepage"				:url "https://awesomewm.org"}
		{:text "awesomewm documentation"		:url "https://awesomewm.org/doc/api"}
		{:text "awesomewm documentation(dev)"	:url "https://awesomewm.org/apidoc"}])

;;;; xplr

(fn xplrLinkBuilder [subpage]
  (.. "https://xplr.dev/en/" subpage))

(local xplrLinks
	   [{:text "xplr homepage" :url "https://xplr.dev/"}
		{:text "xplr documentation" :url (xplrLinkBuilder "")}
		{:text "xplr key-bindings" :url (xplrLinkBuilder :key-bindings)}
		{:text "xplr layouts" :url (xplrLinkBuilder :layout)}
		{:text "xplr mode" :url (xplrLinkBuilder :mode)}
		{:text "xplr message" :url (xplrLinkBuilder :message)}])

;;;; fennel
(fn fennelLinkBuilder [end]
  (.. "https://fennel-lang.org/" end))

(local fennelLinks
	   [{:text "fennel homepage" :url (fennelLinkBuilder "")}
		{:text "fennel tutorial" :url (fennelLinkBuilder :tutorial)}
		{:text "fennel reference" :url (fennelLinkBuilder :reference)}
		{:text :antifennel :url (fennelLinkBuilder :see)}])

;;;; suckless

(fn suckless []
  (fn sl [pre post]
	(.. "https://" pre :suckless.org/ post))

[{:text "homepage"	:url (sl "" "")}
 {:text "dwm"		:url (sl "dwm." "")}
 {:text "dmenu"		:url (sl "tools." "dmenu/")}
 {:text "surf"		:url (sl "surf." "")}
 {:text "tabbed"	:url (sl "tools." "tabbed/")}
 ;;{:text "." :url (suckless "" "")}
 ;;{:text "." :url (suckless "" "")}
 ;;{:text "." :url (suckless "" "")}
 ]
)

;;; page
(local charset "charset=\"UTF-8\"")
;;;; head
(set page.head
	 [(tags.head
	   [(tags.stylesheet :/styles/index.css)
		(tags.title :newtab)
		(tags.meta charset)])])

(local page-info
	   ["In luakit or while using a vim-pluggin/addon in another browser:"
		"use <kbd> f </kbd> to show followable links"
		"use <kbd> F </kbd> to show followable links(opens in new background-tab)"])

;;;; body
(set page.body
 [(tags.div :nav [(tags.navigation util.navorder) (misc.hr)])
  (tags.body
   :content
   [(tags.div
	 :newtab
	 [(tags.div :pagehead [(tags.h1 :WELCOME)])
	  (tags.div :commonLinks
				[(tags.h2 "common links")
				 (tags.linkList quickLinks)])
	  (tags.div :fennel
				[(tags.h2 "fennel links")
				 (tags.linkList fennelLinks)])
	  (tags.div :xplr
				[(tags.h2 :xplr)
				 (tags.linkList xplrLinks)])
	  (tags.div :awesomewm
				[(tags.h2 "awesomewm links")
				 (tags.linkList awesomewmLinks)])
	  (tags.div :suckless
				[(tags.h2 "suckless tools")
				 (tags.linkList (suckless))])
	  (tags.div :info
				[(tags.h2 "luakit keybinds")
				 (tags.p page-info)])])])])


;;; the actual page structure
(set page.foot defaults.foot)
(local thePage
	   {:head page.head
		:body page.body
		:foot page.foot})


(hydra.preview (tags.html thePage))


;;(hydra.writeFile (tags.html thePage) settings.url)
{:content thePage}
