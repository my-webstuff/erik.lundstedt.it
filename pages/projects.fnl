#!/bin/env fennel

(local devmode nil)
(local fennel  (require "fennel"))
(table.insert (or package.loaders package.searchers) fennel.searcher)

(local vars  (require :vars))
(set fennel.path (.. fennel.path ";../hydra/?.fnl"))

;;; imports
(local tags (require :../hydra.tags));; keeping stuff organized
(local hydra (require :../hydra.hydra))
(local misc (require :../hydra.misc))
(local util (require :../util))
(local defaults (require :../defaults))
(local page {});; more effort to get stuff organized

;;;; settings table
(local settings
	   {:title "projects"
		:url "www/pages/projects.html"})

(local gitImg
 {:url "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Git_icon.svg/768px-Git_icon.svg.png"
  :alt "alt"
  :width (/ 768 4)
  :height (/ 768 4)})
;;(fn tags.a [url text ...])






(local charset "charset=\"UTF-8\"")
(set page.head
	 [(tags.head
	   [(tags.stylesheet "/styles/index.css")
		(tags.title "projects")
		(tags.meta charset)])])


(local navigation util.navigation)
;(tset page navigation (util.navigation))
;;(util.log (util.dump navigation) devmode)

;;(set navigation.projects {:text navigation.projects.text :url navigation.projects.url :class "focused"})

(tset navigation settings.title :class :focused)


(util.log "# # # # # # # # # # # # # # # # # # # # # #" devmode)
(util.log (util.dump navigation) devmode)
(util.log "# # # # # # # # # # # # # # # # # # # # # #" devmode)
(util.log (tags.navigation util.navorder) devmode)

(local projects
	   [{:url "https://gitlab.com/Erik.Lundstedt"
		 :text "my gitlab page"
		 :class "gitlab"}
		{:url "https://gitlab.com/Erik.Lundstedt.atom?feed_token=NDU9XZ-tUoBYiX4Jusxa"
		 :text "gitlab atom-feed"
		 :class "atom-feed"}
		{:url "gemini://erik.lundstedt.it"
		 :text "my gemini capsule"
		 :class "capsule"}
		{:url "https://proxy.vulpes.one/gemini/erik.lundstedt.it/"
		 :text "my gemini capsule thru a proxy"
		 :class "capsule proxy"}])




(set page.body
	  [(tags.div :nav [(tags.navigation util.navorder) (misc.hr)])
	   (tags.body :content [(tags.linkList projects)])])



(tset page :foot defaults.foot)
(local thePage
	   {:head page.head
		:body page.body
		:foot page.foot})

;;(hydra.preview (tags.html thePage))
;;(hydra.writeFile (tags.html thePage) "www/pages/projects.html")

{:content thePage}
