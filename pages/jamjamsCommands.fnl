#!/bin/env fennel
(local devmode nil)

;;; stuff to make fennel work

(local fennel (require :fennel))

(table.insert (or package.loaders package.searchers) fennel.searcher)
(local vars  (require :vars))

(set fennel.path (.. fennel.path ";hydra/?.fnl"))

;;; imports
(local tags (require :hydra.tags))

;; keeping stuff organized
(local hydra (require :hydra.hydra))
(local misc (require :hydra.misc))
(local util (require :util))

;;; initialize a table to stuff data in
;;- this sepresents the highest level of the page
(local page {});; more effort to get stuff organized
;;;; new:start using a settings table
(local settings {:title :jamjamscommands :url :www/pages/jamjamsCommands.html})





;;; set some metadata
;;;; variable to store metadata:charset
(local charset "charset=\"UTF-8\"")
;;;; set the "head of the page-data"
(set page.head
	 [(tags.head
	   [(tags.stylesheet :/styles/index.css)
        (tags.title settings.title)
        (tags.meta charset)])])


;;; fetch data from util
(local navigation util.navigation)
;;(tset page navigation (util.navigation))
;;(print (util.dump navigation))
;;;; modify the navigation table
;;(tset navigation settings.title {:text (. navigation settings.title :text)  :url (. navigation settings.title :url)  :class "focused"})

(tset navigation settings.title :class :focused)




(util.log "# # # # # # # # # # # # # # # # # # # # # #" devmode)
(util.log (util.dump navigation) devmode)
(util.log "# # # # # # # # # # # # # # # # # # # # # #" devmode)
(util.log (tags.navigation navigation) devmode)

;;; commands in a table
(local commandTable
;;;; filemanagement
	   {:fileManagement
		{:headers ["command" "short description" "long description"]
		 :rows
		 [["ls"     "list" "lists the contents of a folder to see what is inside of a directory"]
		  ["pwd"    "Print working Directory" "prints the name of your current working directory."]
		  ["cd"  "change directory" "changes the shell's working directory"]
		  ["/"      "-" "root directory"]
		  ["~"      "-" "home directory"]
		  ["mkdir"  "make directory" "used to make folders"]
		  ["touch"  "-" "Used to create a file"]
		  ["rm"     "Remove" "removes files but can also be used to delete a directory"]
		  ["rmdir"  "Remove directory" "Removes a folder IF IT IS EMPTY!"]
		  ["mv"   "move" "We can move or rename files using this"]
		  ["cp"   "Copy" "you can use this to copy a file or folder"]
		  ["find" "-" "Helps you find files/folders matching some file pattern"]
		  ["xdg-open" "-" "will open a file or folder inside or outside of the terminal, might need configuration"]
		  ["jaro" "-" "will open a file or folder inside or outside of the terminal, needs configuration,<br> written and configured in guile scheme lisp<br> can be used as a better xdg-open"]
		  ]
		 }
;;;; scripting
	   :scripting
	   {
		:headers ["command" "short description" "long description"]
		:rows
		[["wc"   "Wordcount" "Counts the words bytes and lines of a file"]
		 ["cat"  "con<b>cat</b>ernate" "Concatenates files, but if only one file is mentioned it prints out the entire file"]
		 ["sort" "-" "Sorts information"]
		 ["uniq" "-" "often used in conjunction with the sort command. Reports or omits repeated data lines"]
		 ["|"    "pipe" "Used for piping to add one command onto another"]
		 [">"    "redirect and overwrite" "Overrites document on the right with new information from document on the left"]
		 [">>"   "redirect and append"    "Appends new informations from the document to the left to the document on the right"]
		 ]}
;;;; utilities
	   :utilities
	   {:headers ["command" "short description" "long description"]
		:rows
		[["whoami"  "who am I?" "prints the username of currently logged in user"]
		 ["echo"    "-" "takes a value and prints it back"]
		 ["clear"   "-" "clears the terminal screen (Control + L in most reminal emulators)"]
		 ["man"     "manual" "is purely informative and provids a manual for any command, shows output in <code>man</code>"]
		 ["apropos" "man by keyword" "Searches through man pages' descriptions for instances of a given keyword."]
		 ["date"    "-" "Prints out the current date and time"]
		 ["time"    "-" "meassures the time a command takes to complete"]
		 ["more" "simple pager" "used to read contents of a file, use <space> to page down"]
		 ["less" "less is more than more" "used to read the contents of files (Provides a TUI)"]
		 ["w3m"  "text based web browser and pager" "used to read the contents of files (Provides a TUI), allso works as a text-only web and gopher browser and as a simle directory browser"]
		 ["head" "-" "Outputs the first 10(or argument) lines of a file"]
		 ["tail" "-" "outputs the last 10 (or argument) lines of a file"]
		 ["diff" "-" "Finds the differences between 2 different files"]
		 ]}
;;;; debian
	   :debian
	   {:headers ["command" "short description" "long description"]
		:rows
		[["dpkg" "-" "installs a debian package from a packagename.deb file"]
		 ["apt-get" "Advanced package tool"
		  (.. "(part of a bigger set of apt-* utilities)"
			  "used to (un)install,update and otherwise manage packages,<br>"
			  "dpkg-frontend,"
			  " debian derivatives only")]
		  ["apt" "Advanced package tool"
		   (.. "used to (un)install,update and otherwise manage packages,"
			   "<br>"
			   "debian derivatives only, modern alternative to apt-get")]
		  ["aptitude" "better debian packagemanager"
		   (..
			"a better packagemanager for debian and "
			"<br>"
			"derivatives that comes with a tui and advanced functions<br>"
			"it even has a builtin game of minesweeper to play while you wait")]]}

;;;; arch
	   :arch
	   {:headers ["command" "argument(s)" "long description"]
		:rows
		[["pacman" "PACkage MANager" "standard arch PACkage MANager.<br> needs sudo"]
		 ["pacman " "-S " "installs a package from the arch repositories"]
		 ["pacman " "-Syyu" "peform system update(excluding AUR)"]
		 ["paru"   " " "AUR wrapper written in rust, asks for sudo when needed"]
		 ["paru"    "-S " "installs a package from the arch repositories or from the AUR"]
		 ["paru"    "-Syyu" "peform system update(including AUR)"]]}})

;;; precreate structure
(local commands
	   [
		(tags.div
		 "grid-item filemanagement"
		 [(tags.h2 "filemanagement")
		  (tags.tbl.table commandTable.fileManagement)])
		(tags.div
		 "grid-item scripting"
		 [(tags.h2 "scripting")
		  (tags.tbl.table commandTable.scripting)])
		(tags.div
		 "grid-item utils"
		 [(tags.h2 "utils")
		  (tags.tbl.table commandTable.utilities)])
		(tags.div "grid-item debian"
				  [(tags.h2 "debian")
				   (tags.tbl.table commandTable.debian)])
		(tags.div "grid-item arch"
				  [(tags.h2 "arch")
				   (tags.tbl.table commandTable.arch)])])



;;; put data in page table
(set page.body
	 [(tags.div :nav [(tags.navigation util.navorder) (misc.hr)])
	  (tags.body :content [(tags.div :grid-container commands)])])


;;; stuff data in final table
;; this is the table that actually stores most of the structure of the page
;;


(local defaults (require :defaults))
(set page.foot defaults.foot)
(local thePage
	   {:head page.head
		:body page.body
		:foot page.foot})


(hydra.preview (tags.html thePage))


;;(hydra.writeFile (tags.html thePage) (. settings :url))
{:content thePage}
