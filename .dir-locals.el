;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((emacs-lisp-mode . ((lua-indent-level . 4)
					 (indent-tabs-mode . t)
					 (tab-width . 4)))
 (fennel-mode . ((mode . outline-minor)
				 (slime-backend . "/home/erik/.luarocks/bin/fennel")
				 (firestarter . "./run;notify-send RUN done! ")
				 (whitespace-mode . t)))
 (less-css-mode . ((firestarter . "./run;notify-send RUN done! "))))
