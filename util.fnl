#!/usr/bin/env fennel

(local fennel  (require :fennel))
(table.insert (or package.loaders package.searchers) fennel.searcher)

(local vars  (require :vars))
(set fennel.path (.. fennel.path ";" vars.hydrapath "/?.fnl"))
;;			(tags.div "navigation" [(tags.linkList navigation)])

(local tags (require :hydra.tags))

;; keeping stuff organized
(local hydra (require :hydra.hydra))
(local misc (require :hydra.misc))


(local home (os.getenv "HOME"))


(local user
	   {:email "mailto:erik@lundstedt.it"
		:name "Erik Lundstedt"
		:git
		{:url "https://gitlab.com/Erik.Lundstedt"
		 :text "my gitlab page"
		 :class "gitlab"}})

(local util
	   {:greeting ["hello " "world" "!"]
		:navigation
		{:home
		 {:text "home"
		  :url "/index.html"
		  :class "home"}
		 :projects
		 {:text "my projects"
		  :url "/pages/projects.html"
		  :class "projects"}
		 ;;points to "external page thats actually hosted on the same server"
		 ;; :git {:text "gitea" :url "/git/" :class "gitea"}
		 ;; :template
		 ;; {
		 ;;  :text "template"
		 ;;  :url "/pages/template.html"
		 ;;  :template "template"
		 ;;  }
		 :jamjamscommands
		 {:text "jamjamscommands"
		  :url "/pages/jamjamsCommands.html"
		  :class "jamjam"}
		 :newtab {
		  :text "useful links"
		  :url "/pages/newtab.html"
		  :class "links"}
		 :blogs {
		  :text "blogs"
		  :url "/blogs/blogs.html"
		  :class "blogs"} }
		:navorder []
		:author user})
(let [navorder
	  [util.navigation.home
	   util.navigation.projects
	   util.navigation.newtab
	   util.navigation.jamjamscommands
	   ;;		 util.navigation.git
	   util.navigation.blogs
	   ;;         (. util.navigation :home)
	   ;;         (. util.navigation :home)
	   ]]
  (set util.navorder navorder))

(fn util.message [format ...]
  (print (string.format format ...)))

(fn util.log [text enabled]
  (if (not= enabled nil)
      (print text)))

(fn util.dump* [o]
  (if
   (= (type o) :table)
   (do
	 (var s "{ ")
	 (each [k v (pairs o)]
	   (when (not= (type k) :number)
		 (set-forcibly! k (.. "\"" k "\"")))
	   (set s (.. s "[" k "] = " (util.dump v) ",\n")))
	 (.. s "}\n "))
   (tostring o)))

(set util.dump fennel.view)
(set util.tcat table.concat)

util
