#!/usr/bin/env fennel
(local fennel  (require "fennel"))
(table.insert (or package.loaders package.searchers) fennel.searcher)
(local vars  (require :vars))

(set fennel.path (.. fennel.path ";hydra/?.fnl"))

(local tags (require :hydra.tags));; keeping stuff organized
(local hydra (require :hydra.hydra))
(local misc (require :hydra.misc))
(local util (require :util))
(local tcat util.tcat)

(local defaults {})
;;; code:
(local imgBaseUrl "https://gitlab.com/Erik.Lundstedt/dotconfigfiles/raw/master/.config/icons/")


(tset defaults :css
	  {:index (tags.stylesheet "/styles/index.css")
	   :mvp "<link rel=\"stylesheet\" href=\"https://unpkg.com/mvp.css@1.9.0/mvp.css\">"
	   })
;;(fn tags.img [url alt width height]
;;(tags.img (.. imgBaseUrl "aperture.svg") "text" 20 20 "hello world")
(set defaults.foot
	  [(tags.footer
		[(tags.div
		  "footer"
		  [(tags.div "title" [(misc.hr)])
		   (tags.div
			"contact"
			[(tags.htag 3
			  (tcat
			   [(tags.img (.. imgBaseUrl "gitlab.svg")  "code" 20 20 "")
				(tags.a "https://gitlab.com/my-webstuff/" "source" [])]))
;;			 ["<a href=\"http://validator.w3.org/check.cgi?url=https%3A//erik.lundstedt.it/feed.atom\"><img src=\"valid-atom.png\" alt=\"[Valid Atom 1.0]\" title=\"Validate my Atom 1.0 feed\" /></a>"]
			 (tags.htag 3
			  (tcat
			   [(tags.img (.. imgBaseUrl "rss.svg") "atom feed" 20 20 "")
				(tags.a "https://erik.lundstedt.it/updates.atom" "atom news feed" [])
				]))])])])])

;;				(tags.htag 4 (.. " source: " util.author.name))

;;  "creates the equivalent html-tag. this is a link"
;;  (.. "\t\t" "<img src=\"" url "\" alt=\"" alt "\" width=\"" width "\" height=\"" height "\">\n"))



defaults
