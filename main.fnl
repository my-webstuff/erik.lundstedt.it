#!/usr/bin/env fennel

(local fennel (require :fennel))

(table.insert (or package.loaders package.searchers) fennel.searcher)
(local vars  (require :vars))
(local util (require :util))

(set fennel.path (.. fennel.path ";hydra/?.fnl;hercules/?.fnl;blogs/?.fnl"))
(local tags (require  :hydra.tags));; keeping stuff organized
(local hydra (require :hydra.hydra))
(local misc (require  :hydra.misc))


(local feed (require :hercules.feed))
(local hercules (require :hercules.hercules))
(local uuids (require :uuids))
(local atom (require :atom))

(local index (require :index))

(local newtab (require :pages.newtab))
(local projects (require :pages.projects))
(local jamjamsCommands (require :pages.jamjamsCommands))


(local blogs (require :blogs.blogs))
(local testblog (require :blogs.testblog))
(local vampire-survivors (require :blogs.vampire-survivors-on-steamdeck))

(fn inBasedir [path] (.. "../html" path))



(fn lessc [infile outfile]
  (fn getHostname []
    "gets system hostname by running the
hostname program and parsing its output
https://gist.github.com/h1k3r/089d43771bdf811eefe8
"
    (local prog (io.popen :/bin/hostname))
    (local tmphostname (or (prog:read :*a) :err))
    (prog:close)
    (local hostname (string.gsub tmphostname "\n$" ""))
    hostname)

  (match (getHostname)
    :archLaptop
    (os.execute (table.concat [:clessc infile :-o outfile :--format] " "))
    :ArchcraftPC
    (os.execute (table.concat [:clessc infile :-o outfile :--format] " "))
    _ (os.execute (table.concat [:lessc infile outfile] " ")))
  (print "lessc done"))



;;; write the files
(hydra.writeFile (tags.html index.content) (inBasedir util.navigation.home.url))
;;;; pages
(hydra.writeFile (tags.html jamjamsCommands.content) (inBasedir util.navigation.jamjamscommands.url))
(hydra.writeFile (tags.html projects.content) (inBasedir util.navigation.projects.url))
(hydra.writeFile (tags.html newtab.content) (inBasedir util.navigation.newtab.url))
;;;; blogs
(hydra.writeFile (tags.html blogs.content) (inBasedir "/blogs/blogs.html"))
(hydra.writeFile (tags.html testblog.content) (inBasedir "/blogs/testblog.html"))
(hydra.writeFile (tags.html vampire-survivors.content) (inBasedir "/blogs/vampire-survivors.html"))
;;;; atom feed(s)
(hercules.writeFile atom.updates (inBasedir "/updates.atom"))
;;(hercules.writeFile atom.updates (inBasedir "/updates.atom"))
;;;; less stylesheet(s)
(lessc "index.less" (inBasedir "/styles/index.css"))
(lessc "blogs/blog.less" (inBasedir "/styles/blogs.css"))
;;; end;

;;(hercules.preview atom.updates)
