#!/usr/bin/env fennel
(local feed (require :hercules.feed))
(local hercules (require :hercules.hercules))
(local testblog (require :blogs.testblog))
(local vampire-survivors (require :blogs.vampire-survivors-on-steamdeck))
;;(local uuids (require :uuids))
(local feeds {})
;;; code:

(fn uuidwrap [uuid]
  (.. "urn:uuid:" uuid))

(local authors {:erik {:name "Erik Lundstedt" :email "erik@lundstedt.it"}})



(local newsfeeds
	   {:news
		{:props
		 (feed.props
		  {:title :news
		   :subtitle "my newsfeed"
		   :link "https://erik.lundstedt.it/index.html"
		   :location "https://erik.lundstedt.it/updates.atom"
		   :updated {:year 2022 :month 6 :day 24}
		   :id (uuidwrap :6c3b1063-5946-45ea-a135-a58e6935bde0)
		   :author authors.erik})
		 :entries
		 [(feed.entry
		   {:title "adding atom feed to my website"
			:subtitle "adding atom feed to my website"
			:link "https://erik.lundstedt.it"
			:summary ["I added an atom feed to my website"]
			:published {:year 2022 :month 6 :day 22}
			:id (uuidwrap :68a3a4b1-cdd2-483e-b8b7-44addea54187)
			:content ["this is " "pretty cool!"]
			:author authors.erik})
		  testblog.post.feed
		  vampire-survivors.post.feed]}
		:docs
		{:props
		 (feed.props
		  {:title :docs
		   :subtitle "atom feed of updates to my documentation pages"
		   :link "https://erik.lundstedt.it/index.html"
		   :location "https://erik.lundstedt.it/docs.atom"
		   :updated {:year 2022 :month 7 :day 12}
		   :id (uuidwrap :b77c90d6-d891-485b-8759-7dc72ce4daf9)
		   :author authors.erik})}})

(set feeds.updates (feed.build newsfeeds.news.props newsfeeds.news.entries))

;;(set feeds.docs
;;      (feed.build newsfeeds.docs.props newsfeeds.docs.entries))

;;{:updates }
feeds
