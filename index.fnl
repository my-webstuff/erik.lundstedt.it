#!/usr/bin/env fennel

(local devmode nil)
(local fennel (require :fennel))
(table.insert (or package.loaders package.searchers) fennel.searcher)
(local vars (require :vars))
(local util (require :util))

(set fennel.path (.. fennel.path ";hydra/?.fnl"))
(local tags (require :hydra.tags))

;; keeping stuff organized
(local hydra (require :hydra.hydra))
(local misc (require :hydra.misc))
(local defaults (require :defaults))

(local page {})

;;;; settings table
(local settings
	   {:title "home"
		:url "www/index.html"})
;; more effort to get stuff organized

(local charset "charset=\"UTF-8\"")
(set page.head
	 [(tags.head
	   [defaults.css.index
		;;(tags.stylesheet "/styles/index.css")
		(tags.title :landingPage)
		(tags.meta charset)
		(tags.feedlink
		 {:url "https://erik.lundstedt.it/updates.atom"
		  :title "atom news feed"})])])

(local navigation util.navigation)
;(tset page navigation (util.navigation))
(util.log (util.dump navigation))

;;(set navigation.home {:text navigation.home.text :url navigation.home.url :class :focused})
(tset navigation settings.title :class :focused)

(util.log "# # # # # # # # # # # # # # # # # # # # # #" devmode)
(util.log (util.dump navigation) devmode)
(util.log "# # # # # # # # # # # # # # # # # # # # # #" devmode)
(util.log (tags.navigation navigation) devmode)

(set page.body
	 [(tags.body
	   :content
	   [(tags.div :nav [(tags.navigation util.navorder) (misc.hr)])
		(tags.h1tag (.. "(print (table.concat greeting))"))
		(tags.h2tag (.. ";;---> " (table.concat util.greeting)))])])

;;(hydra.preview (tags.html page))
;;(hydra.writeFile (tags.html page) "www/index.html")

(set page.foot defaults.foot)
(local thePage {:head page.head :body page.body :foot page.foot})

{:content thePage}
