#!/usr/bin/env fennel
(local devmode nil)

;;; stuff to make fennel work

(local fennel (require :fennel))

(table.insert (or package.loaders package.searchers) fennel.searcher)

(local vars  (require :vars))
(set fennel.path (.. fennel.path ";hydra/?.fnl"))


;;; imports
(local tags (require :hydra.tags))

;; keeping stuff organized
(local hydra (require :hydra.hydra))
(local misc (require :hydra.misc))
(local util (require :util))

;;; initialize a table to stuff data in
;;- this sepresents the highest level of the page
(local page {});; more effort to get stuff organized
;;;; settings table
(local settings
	   {:title "template"
		:url "www/pages/template.html"})


;;; set some metadata
;;;; variable to store metadata:charset
(local charset "charset=\"UTF-8\"")
;;;; set the "head of the page-data"
(set page.head
	  [(tags.head
		[(tags.stylesheet :/styles/index.css)
		 (tags.title settings.title)
		 (tags.meta charset)])])


;;; fetch data from util
(local navigation util.navigation)
;;(tset page navigation (util.navigation))
;;(print (util.dump navigation))
;;;; modify the navigation table

;;(set navigation.template {:text navigation.home.text :url navigation.home.url :class :focused})

(tset navigation settings.title :class :focused)


(util.log "# # # # # # # # # # # # # # # # # # # # # #" devmode)
(util.log (util.dump navigation) devmode)
(util.log "# # # # # # # # # # # # # # # # # # # # # #" devmode)
(util.log (tags.navigation navigation) devmode)


;;; example links table for use with tags.linklist

(local links
	   [{:url "/index.html"				;url to open when link is followed
		 :text "home"					;description/text to show for the link
		 ;;:class ""                      ;optional class property
		 }])

;;; put data in page table
(tset page :body
	  [(tags.div :nav [(tags.navigation util.navorder) (misc.hr)])
	   (tags.body :content [(tags.linkList links devmode)])])


;;; stuff data in final table
;; this is the table that actually stores most of the structure of the page
;;
(local defaults (require :defaults))
(tset page :foot defaults.foot)
(local thePage {:head page.head :body page.body :foot page.foot})



;;(hydra.preview (tags.html thePage))
(print (tags.html thePage))

;;(hydra.writeFile (tags.html thePage) (. settings :url))

{:content thePage}
