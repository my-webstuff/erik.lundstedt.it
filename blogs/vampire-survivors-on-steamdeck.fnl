#!/usr/bin/env fennel
(local devmode nil)
(local fennel  (require "fennel"))
(table.insert (or package.loaders package.searchers) fennel.searcher)

(set fennel.path (.. fennel.path ";../hydra/?.fnl"))
;;(local vars  (require :../vars))

;;; imports
(local tags (require :hydra.tags));; keeping stuff organized
(local hydra (require :hydra.hydra))
(local misc (require :hydra.misc))
(local util (require :util))
(local defaults (require :defaults))

(local blogpost (require :blogs.blogpost))
(local page {});; more effort to get stuff organized
;;; config:

(local settings {:title "blogs"})

;;; code:

(local charset "charset=\"UTF-8\"")

(set page.head
     [(tags.head
       [(tags.stylesheet "/styles/index.css")
        (tags.stylesheet "/styles/blogs.css")
        (tags.title "playing Vampire survivors on the steamdeck")
        (tags.meta charset)])])


(local navigation util.navigation)
;(tset page navigation (util.navigation))
(util.log (util.dump navigation) devmode)


(tset navigation settings.title :class "focused")



(util.log "# # # # # # # # # # # # # # # # # # # # # #" devmode)
(util.log (util.dump navigation) devmode)
(util.log "# # # # # # # # # # # # # # # # # # # # # #" devmode)
(util.log (tags.navigation util.navorder) devmode)

(fn game [head desc rating price link]
  (tags.article
   head
   [(tags.h2 head)
	(tags.p desc)
	(tags.p [(table.concat [(misc.bold "Deck verification rating: ") rating] )])
	(tags.p [(table.concat [(misc.bold "Price: ") price "€"] )])
;;	(tags.p [(misc.bold (table.concat ["Price: " price " USD"]))])
	(tags.a link (.. head " on steam") {})
	]))

(local ratings
	   {0 (misc.span "Unknown"		"color:red;")
		1 (misc.span "Unverified"	"color:dark-blue;")
		2 (misc.span "Playable"	"color:yellow;")
		3 (misc.span "Verified"	"color:green;")
		})

(local content
	   [(game
		 "Vampire survivors"
		 ["Vampire survivors is a great game that works great booth on the deck and on larger screens."
		  "It is marked as verified for steamdeck and it shows as it detects the steamdeck and runs optimisations accordingly."
		  "It also has a great default mapping for the controls on the deck."]
		 (. ratings 3)
		 4.99
		 "https://store.steampowered.com/app/1794680/Vampire_Survivors/")])

(local post-arguments
        {:title "vampire-survivors"
         :summary ["Playing vampire survivors on steamdeck is a great experience [15/15]"]
         :subtitle "I didnt think I would like this game as much as I did."
         :content content
         :posted {:year 2022 :month 09 :day 16}
         :author {:name "Erik Lundstedt" :email "erik@lundstedt.it"}
         :id "6fd1b68b-2145-4a73-badd-e863cfcba4b5"
         })
(tset post-arguments :updated {:year (+ post-arguments.posted.year 1) :month 09 :day 01})
(local post (blogpost.new post-arguments))
	   
(set page.body
     [(tags.div
	   "nav"
       [(tags.navigation util.navorder)
        (misc.hr)])
      (tags.body
	   "content"
       [post.blog])])


(set page.foot defaults.foot)
(local thePage
       {:head page.head
        :body page.body
        :foot page.foot})

(hydra.preview (tags.html thePage))
;;(hydra.writeFile (tags.html thePage) "www/pages/projects.html")

{:content thePage :post post}
