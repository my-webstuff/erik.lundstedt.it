#!/bin/env fennel
(local devmode nil)
(local fennel  (require "fennel"))
(table.insert (or package.loaders package.searchers) fennel.searcher)

(set fennel.path (.. fennel.path ";../hydra/?.fnl"))
;;(local vars  (require :../vars))

;;; imports
(local tags (require :hydra.tags));; keeping stuff organized
(local hydra (require :hydra.hydra))
(local misc (require :hydra.misc))
(local util (require :util))
(local defaults (require :defaults))

(local blogpost (require :blogs.blogpost))


(local page {});; more effort to get stuff organized
;;; config
(local settings {:title "blogs"})
;;; code:

(local charset "charset=\"UTF-8\"")

(set page.head
	 [(tags.head
	   [(tags.stylesheet :/styles/index.css)
        (tags.stylesheet :/styles/blogs.css)
        (tags.title :testBlog)
        (tags.meta charset)])])



(local navigation util.navigation)
;(tset page navigation (util.navigation))
(util.log (util.dump navigation) devmode)

(tset navigation settings.title :class "focused")

(util.log "# # # # # # # # # # # # # # # # # # # # # #" devmode)
(util.log (util.dump navigation) devmode)
(util.log "# # # # # # # # # # # # # # # # # # # # # #" devmode)
(util.log (tags.navigation util.navorder) devmode)

(local post
       (blogpost.new
        {:title "testblog"
         :summary ["testing testing and more testing"]
         :subtitle "so this is the subheadding huh?"
         :content ["This is just a test" "dont mind me" "just testing here"]
         :posted {:year 2022 :month 06 :day 24}
         :author {:name "Erik Lundstedt" :email "erik@lundstedt.it"}
         :id "de6e6460-d627-47eb-a9c9-c16a1d5894c8"}))


(tset page :body
      [(tags.div
		"nav"
        [(tags.navigation util.navorder)
         (misc.hr)])
       (tags.body
		"content"
        [post.blog])])


(set page.foot defaults.foot)
(local thePage
       {:head page.head
        :body page.body
        :foot page.foot})

(hydra.preview (tags.html thePage))
;;(hydra.writeFile (tags.html thePage) "www/pages/projects.html")

{:content thePage :post post}
