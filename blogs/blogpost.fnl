#!/bin/env fennel
(local devmode nil)
(local fennel  (require "fennel"))
(table.insert (or package.loaders package.searchers) fennel.searcher)

(local vars  (require :vars))
(set fennel.path (.. fennel.path ";../hydra/?.fnl"))

;;; imports
(local tags (require :hydra.tags));; keeping stuff organized
(local hydra (require :hydra.hydra))
(local misc (require :hydra.misc))
(local util (require :util))
(local feed (require :hercules.feed))
(local defaults (require :defaults))

(local blogpost {})

;;; code:

(fn uuidwrap [uuid] (.. "urn:uuid:" uuid))


(fn blogpost.new
  [post]
;;  title
;;  summary
;;  subtitle
;;  content
;;  author
   ;;id
;;   posted
;;   updated
  {:blog
   (tags.div "post" [
    (tags.div "title" [(tags.h1 post.title)])
    (tags.div "summary" [(tags.p post.summary)])
    (tags.div "subtitle" [(tags.h2 post.subtitle)])
    (tags.div "blogContent" [(tags.p post.content)])
    (tags.div
	 "footer"
	 [(misc.hr)
      (tags.p [(.. "written by: " post.author.name)])
      (tags.p [(.. "email: " (tags.a (.. "mailto:" post.author.email) post.author.email []))])
      (tags.p [(.. "uuid: " post.id)])])])

;;:title
;;:summary
;;:subtitle
;;:content
;;:author
   :feed
   (feed.entry
    {:title post.title
     :link (.. "https://erik.lundstedt.it/blogs/" post.title ".html")
     :location (.. "https://erik.lundstedt.it/updates.atom")
     :summary post.summary
     :content post.content
     :posted post.posted
     :author post.author
     :updated (or post.updated post.posted)
     :id (uuidwrap post.id)
     })})




blogpost
