#!/bin/env fennel
(local devmode nil)
(local fennel  (require "fennel"))
(table.insert (or package.loaders package.searchers) fennel.searcher)

(local vars  (require :vars))
(set fennel.path (.. fennel.path ";../hydra/?.fnl"))

;;; imports
(local tags (require :hydra.tags));; keeping stuff organized
(local hydra (require :hydra.hydra))
(local misc (require :hydra.misc))
(local util (require :util))
(local defaults (require :defaults))
(local page {});; more effort to get stuff organized

;;; code:

(local charset "charset=\"UTF-8\"")

(tset page :head
      [(tags.head
        [(tags.stylesheet "/styles/index.css")
         (tags.stylesheet "/styles/blogs.css")
         (tags.title "blogs")
         (tags.meta charset)])])


(local navigation util.navigation)
;(tset page navigation (util.navigation))
(util.log (util.dump navigation) devmode)

;;(set navigation.blogs {:text (. navigation.projects :text) :url (. navigation.projects :url) :class "focused" })

(util.log "# # # # # # # # # # # # # # # # # # # # # #" devmode)
(util.log (util.dump navigation) devmode)
(util.log "# # # # # # # # # # # # # # # # # # # # # #" devmode)
(util.log (tags.navigation util.navorder) devmode)

(local blogs
       [{:url "vampire-survivors.html"
         :text "playing vampire survivors on the steamdeck"
         :class "blogPost"}
		{:url "testblog.html"
         :text "just a test"
         :class "blogPost"}
        ;; {
        ;;  :url "howIMadeIt.html"
        ;;  :text "how this website is made"
        ;;  :class "blogPost"
        ;;  }
        ])

(local backlinks
       [{:url "https://erik.lundstedt.it/blogs/blogs.html"
         :text "back"
         :class "backlink"}])



(set page.body
     [(tags.div
	   "nav"
       [(tags.navigation util.navorder)
        (misc.hr)])
      (tags.body
	   "content"
       [(tags.linkList blogs)])])


(set page.foot defaults.foot)
(local thePage
       {:head page.head
        :body page.body
        :foot page.foot})

(hydra.preview (tags.html thePage))
;;(hydra.writeFile (tags.html thePage) "www/pages/projects.html")

{:content thePage}
